// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;

import '../pages/homepage.dart' as _i1;
import '../pages/movie_add_or_update.dart' as _i2;

class AppRouter extends _i3.RootStackRouter {
  AppRouter([_i4.GlobalKey<_i4.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    Homepage.name: (routeData) {
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.Homepage());
    },
    MovieAddorUpdate.name: (routeData) {
      final args = routeData.argsAs<MovieAddorUpdateArgs>();
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i2.MovieAddorUpdate(key: args.key, data: args.data));
    }
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig(Homepage.name, path: '/'),
        _i3.RouteConfig(MovieAddorUpdate.name, path: '/movie_add_update')
      ];
}

/// generated route for
/// [_i1.Homepage]
class Homepage extends _i3.PageRouteInfo<void> {
  const Homepage() : super(Homepage.name, path: '/');

  static const String name = 'Homepage';
}

/// generated route for
/// [_i2.MovieAddorUpdate]
class MovieAddorUpdate extends _i3.PageRouteInfo<MovieAddorUpdateArgs> {
  MovieAddorUpdate({_i4.Key? key, required Map<dynamic, dynamic> data})
      : super(MovieAddorUpdate.name,
            path: '/movie_add_update',
            args: MovieAddorUpdateArgs(key: key, data: data));

  static const String name = 'MovieAddorUpdate';
}

class MovieAddorUpdateArgs {
  const MovieAddorUpdateArgs({this.key, required this.data});

  final _i4.Key? key;

  final Map<dynamic, dynamic> data;

  @override
  String toString() {
    return 'MovieAddorUpdateArgs{key: $key, data: $data}';
  }
}
