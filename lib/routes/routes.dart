import 'package:auto_route/annotations.dart';
import 'package:briix_test/pages/homepage.dart';
import 'package:briix_test/pages/movie_add_or_update.dart';

// @CupertinoAutoRouter
// @AdaptiveAutoRouter
// @CustomAutoRouter
@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: Homepage, initial: true),
    AutoRoute(page: MovieAddorUpdate, path: '/movie_add_update'),
  ],
)
class $AppRouter {}
