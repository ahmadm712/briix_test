import 'dart:async';

import 'package:briix_test/injection.dart';
import 'package:briix_test/models/movie_model.dart';
import 'package:briix_test/routes/routes.gr.dart';
import 'package:briix_test/shared/global_functions.dart';
import 'package:briix_test/shared/style.dart';
import 'package:briix_test/state/movie_store.dart';
import 'package:briix_test/widgets/movie_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  final movieState = locator.get<MovieStore>();
  List<MovieModel> dataSearch = [];

  final router = locator.get<AppRouter>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Timer.periodic(const Duration(seconds: 3), (timer) {
      setState(() {
        dataSearch = movieState.movies;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies Collection'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Observer(
        builder: (context) {
          return ScrollConfiguration(
            behavior: const ScrollBehavior().copyWith(overscroll: false),
            child: RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  dataSearch = movieState.movies;
                });
              },
              child: SingleChildScrollView(
                padding:
                    GlobalFunctions.screenSize(context: context).width > 700
                        ? const EdgeInsets.symmetric(horizontal: 100)
                        : const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    formSearch(data: movieState.movies),
                    SizedBox(
                      height:
                          GlobalFunctions.screenSize(context: context).height *
                              0.9,
                      child: RefreshIndicator(
                        onRefresh: () async {
                          setState(() {
                            dataSearch = movieState.movies;
                          });
                        },
                        child: ScrollConfiguration(
                          behavior: const ScrollBehavior()
                              .copyWith(overscroll: false),
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: dataSearch.length,
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            itemBuilder: (context, index) {
                              final movie = dataSearch[index];
                              return MovieCard(
                                movie: movie,
                                index: index,
                              );
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          router.push(
            MovieAddorUpdate(
              data: const {
                'status': 'add',
              },
            ),
          );
        },
        tooltip: 'Add Movie',
        backgroundColor: kColorPrimary,
        child: const Icon(Icons.add),
      ),
    );
  }

  Container formSearch({required List<MovieModel> data}) {
    return Container(
      height: 42,
      margin: const EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(20)),
      child: TextField(
        onChanged: (value) {
          _runFilter(value, data);
        },
        onSubmitted: (value) {},
        decoration: const InputDecoration(
          border: InputBorder.none,
          icon: Icon(
            Icons.search,
            size: 17,
          ),
          hintText: 'Search By Title ...',
        ),
      ),
    );
  }

  void _runFilter(String enteredKeyword, List<MovieModel> dataArgmn) {
    List<MovieModel> results = [];
    if (enteredKeyword.isEmpty) {
      setState(() {
        results = dataArgmn;
      });
    } else {
      setState(() {
        results = dataArgmn
            .where(
              (mov) =>
                  mov.title.toLowerCase().contains(
                        enteredKeyword.toLowerCase(),
                      ) ||
                  mov.director.toLowerCase().contains(
                        enteredKeyword.toLowerCase(),
                      ) ||
                  mov.summary.toLowerCase().contains(
                        enteredKeyword.toLowerCase(),
                      ),
            )
            .toList();
      });
    }

    // Refresh the UI
    setState(() {
      dataSearch = results;
    });
  }
}
