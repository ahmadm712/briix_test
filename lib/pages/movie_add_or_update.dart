import 'dart:developer' as dev;
import 'dart:math';

import 'package:briix_test/injection.dart';
import 'package:briix_test/models/movie_model.dart';
import 'package:briix_test/routes/routes.gr.dart';
import 'package:briix_test/shared/global_functions.dart';
import 'package:briix_test/shared/style.dart';
import 'package:briix_test/state/movie_store.dart';
import 'package:briix_test/widgets/formfield.dart';
import 'package:briix_test/widgets/formfield_large.dart';
import 'package:briix_test/widgets/multiplechoices_chip.dart';
import 'package:flutter/material.dart';

class MovieAddorUpdate extends StatefulWidget {
  MovieAddorUpdate({Key? key, required this.data}) : super(key: key);

  Map data;
  @override
  State<MovieAddorUpdate> createState() => _MovieAddorUpdateState();
}

class _MovieAddorUpdateState extends State<MovieAddorUpdate> {
  TextEditingController titleController = TextEditingController();
  TextEditingController directorController = TextEditingController();
  TextEditingController summaryController = TextEditingController();
  late MovieModel movieData;
  final movieState = locator.get<MovieStore>();
  final router = locator.get<AppRouter>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  List<String> genreChoosen = [];
  bool isEditableGenre = false;

  @override
  void initState() {
    super.initState();
    if (widget.data['movie'] != null) {
      movieData = widget.data['movie'];
      titleController = TextEditingController(text: movieData.title);
      directorController = TextEditingController(text: movieData.director);
      summaryController = TextEditingController(text: movieData.summary);
      genreChoosen = movieData.genres;
    }
  }

  @override
  void dispose() {
    super.dispose();
    titleController.dispose();
    directorController.dispose();
    summaryController.dispose();
  }

  void clearField() {
    titleController.clear();
    directorController.clear();
    summaryController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: (widget.data['status'] == 'add')
              ? const Text('Add Movie')
              : const Text('Update Movie'),
          actions: [
            IconButton(
              tooltip: (widget.data['status'] == 'add') ? 'Save' : 'Update',
              onPressed: () {
                if (widget.data['status'] == 'add') {
                  if (GlobalFunctions.validate(
                      context: context, formkey: formKey)) {
                    setState(() {
                      movieState.addMovie(
                        movie: MovieModel(
                          id: Random().nextInt(1000),
                          title: titleController.text,
                          director: directorController.text,
                          summary: summaryController.text,
                          genres: genreChoosen,
                        ),
                      );
                    });
                    GlobalFunctions.scaffoldMessage(
                        context: context,
                        message: 'Add Movie Success',
                        color: Colors.green);
                    router.pop(
                        // const Homepage(),
                        // predicate: (route) => false,
                        );
                    setState(() {});
                  }
                } else if (widget.data['status'] == 'update') {
                  setState(() {
                    movieState.updateMovie(
                      index: widget.data['index'],
                      movie: MovieModel(
                        id: Random().nextInt(1000),
                        title: titleController.text,
                        director: directorController.text,
                        summary: summaryController.text,
                        genres: genreChoosen,
                      ),
                    );
                  });
                  GlobalFunctions.scaffoldMessage(
                      context: context,
                      message: 'Update Success',
                      color: kColorPrimary);
                  router.pushAndPopUntil(
                    const Homepage(),
                    predicate: (route) => false,
                  );
                }
              },
              icon: const Icon(
                Icons.save,
              ),
            ),
            (widget.data['status'] == 'update')
                ? IconButton(
                    tooltip: 'Remove',
                    onPressed: () {
                      setState(() {
                        movieState.deleteMovie(id: movieData.id);
                      });
                      GlobalFunctions.scaffoldMessage(
                          context: context,
                          message: 'Remove Movie Success',
                          color: Colors.red);
                      router.pushAndPopUntil(
                        const Homepage(),
                        predicate: (route) => false,
                      );
                    },
                    icon: const Icon(
                      Icons.delete,
                    ),
                  )
                : Container(),
          ]),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              FormFieldComponent(
                  controller: titleController,
                  validatorText: 'Please Fill the title',
                  labelText: 'Title'),
              const SizedBox(
                height: 16,
              ),
              FormFieldComponent(
                  controller: directorController,
                  validatorText: 'Please Fill the director',
                  labelText: 'Director'),
              const SizedBox(
                height: 16,
              ),
              FormFieldLargeComponent(
                  controller: summaryController,
                  validatorText: 'Please Fill the Summary',
                  labelText: 'Summary'),
              const SizedBox(
                height: 16,
              ),
              MultiSelectChip(
                (widget.data['movie'] != null) && !isEditableGenre
                    ? movieData.genres
                    : isEditableGenre
                        ? const ['Action', 'Animation', 'Drama', 'Sci-Fi']
                        : const ['Action', 'Animation', 'Drama', 'Sci-Fi'],
                onSelectionChanged: (selectedList) {
                  setState(() {
                    genreChoosen = selectedList;
                    dev.log(genreChoosen.toString());
                  });
                },
                maxSelection: 4,
              ),
              (widget.data['status'] == 'update')
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          isEditableGenre
                              ? isEditableGenre = false
                              : isEditableGenre = true;
                        });
                      },
                      icon: const Icon(Icons.edit))
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
