import 'package:briix_test/routes/routes.gr.dart';
import 'package:briix_test/state/movie_store.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void setup() {
  // Store
  locator.registerSingleton(MovieStore());
  // Router
  locator.registerSingleton(AppRouter());
}
