import 'package:briix_test/injection.dart';
import 'package:briix_test/models/movie_model.dart';
import 'package:briix_test/routes/routes.gr.dart';
import 'package:briix_test/shared/global_functions.dart';
import 'package:flutter/material.dart';

class MovieCard extends StatelessWidget {
  MovieCard({
    Key? key,
    required this.movie,
    required this.index,
  }) : super(key: key);

  final MovieModel movie;
  final int index;
  final router = locator.get<AppRouter>();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        router.push(
          MovieAddorUpdate(
            data: {'movie': movie, 'status': 'update', 'index': index},
          ),
        );
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.title,
                    style: GlobalFunctions.textTheme(context: context)
                        .headline3!
                        .copyWith(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                  ),
                  Text(
                    movie.director,
                    style: GlobalFunctions.textTheme(context: context)
                        .headline3!
                        .copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.w300,
                            color: Colors.black),
                  ),
                ],
              ),
              Flexible(child: Text(movie.genres.map((e) => e).toString()))
            ],
          ),
        ),
      ),
    );
  }
}
