import 'package:briix_test/shared/global_functions.dart';
import 'package:briix_test/shared/style.dart';
import 'package:flutter/material.dart';

class FormFieldComponent extends StatelessWidget {
  const FormFieldComponent({
    Key? key,
    required this.validatorText,
    required this.labelText,
    required this.controller,
  }) : super(key: key);
  final String validatorText;
  final String labelText;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: (value) {
        if (value!.isEmpty) {
          return validatorText;
        }
        return null;
      },
      decoration: InputDecoration(
        isDense: true,
        hintText: '',
        contentPadding: const EdgeInsets.all(
          16,
        ),
        labelText: labelText,
        labelStyle: GlobalFunctions.textTheme(context: context)
            .subtitle2!
            .copyWith(fontSize: 16, color: const Color(0xff6A6F7A)),
        hintStyle: GlobalFunctions.textTheme(context: context)
            .subtitle2!
            .copyWith(fontSize: 14, color: const Color(0xff6A6F7A)),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(
              color: Color(
                0xffC5C7CB,
              ),
              width: 1),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: Colors.red.shade900,
            width: 1,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: const BorderSide(
            color: Colors.red,
            width: 1,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: kColorPrimary, width: 1),
        ),
      ),
    );
  }
}
