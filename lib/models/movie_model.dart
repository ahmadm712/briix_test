class MovieModel {
  int id;
  String title;
  String director;
  String summary;
  List<String> genres;

  MovieModel({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });
}
