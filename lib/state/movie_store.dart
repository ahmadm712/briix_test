// ignore_for_file: library_private_types_in_public_api

import 'package:briix_test/models/movie_model.dart';
import 'package:mobx/mobx.dart';
part 'movie_store.g.dart';

class MovieStore = _MovieStoreBase with _$MovieStore;

abstract class _MovieStoreBase with Store {
  @observable
  List<MovieModel> movies = [];

  @observable
  List<MovieModel> searchableMovies = [];

  @observable
  bool isLoading = false;

  @action
  void addMovie({required MovieModel movie}) {
    isLoading = true;
    movies.add(movie);
    isLoading = false;
  }

  @action
  void deleteMovie({required int id}) {
    isLoading = true;
    movies.removeWhere((element) => element.id == id);
    isLoading = false;
  }

  @action
  void updateMovie({required int index, required MovieModel movie}) {
    isLoading = true;
    movies[index] = movie;
    isLoading = false;
  }

  @action
  void searchMovie(String enteredKeyword) {
    List<MovieModel> results = [];
    if (enteredKeyword.isEmpty) {
      results = movies;
    } else {
      results = movies
        ..where(
          (mov) =>
              mov.title.toLowerCase().contains(
                    enteredKeyword.toLowerCase(),
                  ) ||
              mov.director.toLowerCase().contains(
                    enteredKeyword.toLowerCase(),
                  ) ||
              mov.summary.toLowerCase().contains(
                    enteredKeyword.toLowerCase(),
                  ),
        ).toList();
    }

    searchableMovies = results;
  }
}
