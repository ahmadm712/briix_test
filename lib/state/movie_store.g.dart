// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieStore on _MovieStoreBase, Store {
  late final _$moviesAtom =
      Atom(name: '_MovieStoreBase.movies', context: context);

  @override
  List<MovieModel> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(List<MovieModel> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  late final _$searchableMoviesAtom =
      Atom(name: '_MovieStoreBase.searchableMovies', context: context);

  @override
  List<MovieModel> get searchableMovies {
    _$searchableMoviesAtom.reportRead();
    return super.searchableMovies;
  }

  @override
  set searchableMovies(List<MovieModel> value) {
    _$searchableMoviesAtom.reportWrite(value, super.searchableMovies, () {
      super.searchableMovies = value;
    });
  }

  late final _$isLoadingAtom =
      Atom(name: '_MovieStoreBase.isLoading', context: context);

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  late final _$_MovieStoreBaseActionController =
      ActionController(name: '_MovieStoreBase', context: context);

  @override
  void addMovie({required MovieModel movie}) {
    final _$actionInfo = _$_MovieStoreBaseActionController.startAction(
        name: '_MovieStoreBase.addMovie');
    try {
      return super.addMovie(movie: movie);
    } finally {
      _$_MovieStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteMovie({required int id}) {
    final _$actionInfo = _$_MovieStoreBaseActionController.startAction(
        name: '_MovieStoreBase.deleteMovie');
    try {
      return super.deleteMovie(id: id);
    } finally {
      _$_MovieStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateMovie({required int index, required MovieModel movie}) {
    final _$actionInfo = _$_MovieStoreBaseActionController.startAction(
        name: '_MovieStoreBase.updateMovie');
    try {
      return super.updateMovie(index: index, movie: movie);
    } finally {
      _$_MovieStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void searchMovie(String enteredKeyword) {
    final _$actionInfo = _$_MovieStoreBaseActionController.startAction(
        name: '_MovieStoreBase.searchMovie');
    try {
      return super.searchMovie(enteredKeyword);
    } finally {
      _$_MovieStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movies: ${movies},
searchableMovies: ${searchableMovies},
isLoading: ${isLoading}
    ''';
  }
}
